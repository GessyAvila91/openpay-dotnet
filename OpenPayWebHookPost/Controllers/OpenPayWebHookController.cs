﻿using System;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Newtonsoft.Json.Converters;

using System.Web.Script.Serialization;



namespace OpenPayWebHookPost.Controllers {
    public class OpenPayWebHookController : Controller {
        // GET
        public ActionResult Index() {
            return View();
        }
        
        [HttpPost]
        public ActionResult WebHookPost(String jsonHook = null)
        {
            
            try
            {
                var jsonHookArray = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(jsonHook);
                
                if (jsonHookArray.Count > 0) {
                    ViewBag.type          = jsonHookArray["type"];
                    ViewBag.event_date    = jsonHookArray["event_date"]; 
                    ViewBag.transaction   = jsonHookArray["transaction"];
                    ViewBag.amount        = ViewBag.transaction["amount"];
                    ViewBag.id            = ViewBag.transaction["id"];
                    ViewBag.error_message = ViewBag.transaction["error_message"];
                    
                    return View();
                }
            }
            catch (Exception exc)
            {
                
            }

            return View();
        }
        
    }
    
}
